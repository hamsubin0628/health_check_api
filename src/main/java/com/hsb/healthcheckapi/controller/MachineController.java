package com.hsb.healthcheckapi.controller;

import com.hsb.healthcheckapi.model.MachineRequest;
import com.hsb.healthcheckapi.model.MachineStaticsResponse;
import com.hsb.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine(@RequestBody MachineRequest request){
        machineService.setMachine(request);

        return "OK";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatics(){
        return machineService.getStatics();
    }
}
