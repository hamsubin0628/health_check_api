package com.hsb.healthcheckapi.repository;

import com.hsb.healthcheckapi.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository<Health, Long> {
}
