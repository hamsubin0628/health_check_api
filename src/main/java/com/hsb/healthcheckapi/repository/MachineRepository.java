package com.hsb.healthcheckapi.repository;

import com.hsb.healthcheckapi.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineRepository extends JpaRepository<Machine, Long> {
}
