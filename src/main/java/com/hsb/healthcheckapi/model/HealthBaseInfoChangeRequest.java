package com.hsb.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthBaseInfoChangeRequest {
    private String name;
    private Boolean isChronicDisease;
}
