package com.hsb.healthcheckapi.model;

import com.hsb.healthcheckapi.enums.HealthStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthRequest {
    private String name;

    @Enumerated(value = EnumType.STRING)
    private HealthStatus healthStatus;

    private String ectMemo;

    private Boolean isChronicDisease;

}
