package com.hsb.healthcheckapi.service;

import com.hsb.healthcheckapi.entity.Machine;
import com.hsb.healthcheckapi.model.MachineRequest;
import com.hsb.healthcheckapi.model.MachineStaticsResponse;
import com.hsb.healthcheckapi.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request){
        Machine addData = new Machine();
        addData.setMachineName(request.getMachineName());
        addData.setMachinePrice(request.getMachinePrice());
        addData.setDateBuy(request.getDateBuy());

        machineRepository.save(addData);
    }

    public MachineStaticsResponse getStatics(){
        MachineStaticsResponse response = new MachineStaticsResponse();

        List<Machine> originList =machineRepository.findAll();

        double totalPrice = 0D;
        for (Machine machine : originList) {
            totalPrice += machine.getMachinePrice();
            // 풀어서 쓰면 totalPrice = totalPrice + machine.getMachinePrice();
        }

        double avgPrice = totalPrice / originList.size();

        response.setTotalPrice(totalPrice);
        response.setAveragePrice(avgPrice);

        return response;
    }
}
